﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="task.aspx.cs" Inherits="HRA.task" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all"/>
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all"/>
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    
    
    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

    <link href="myCss.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
   <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading card-heading-button">
                    <asp:HyperLink Text="Back" ID="backButtonHyperlink" CssClass="btn btn--radius btn--green btn-back" NavigateUrl="/UserDashboard.aspx" runat="server"/>

                </div>
                <div class="card-body">
                    <h2 class="title">Task Registration</h2>
                    <form runat="server">
                        <div class="input-group">
                            
                            <asp:TextBox ID="taskTitle" CssClass="input--style-1" runat="server" type="text" placeholder="Task Title" name="name"/>
                        </div>

                        <div class="input-group">
                            
                            <asp:TextBox ID="taskDescription" CssClass="input--style-1"  type="text" placeholder="Task Description" name="name" runat="server" />
                        </div>
                        <div class="input-group">
                            
                            <asp:TextBox ID="textMaxNoOfEmp" CssClass="input--style-1" TextMode="Number"  type="text" placeholder="Maximum Number Of Employees" name="name" runat="server" />
                        </div>
                        <div class="input-group">
                            
                            <asp:TextBox ID="expairyDate" CssClass="input--style-1" TextMode="Date"  type="text" placeholder="Task Expairy Date" name="name" runat="server" />
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <p>Department Name</p>
                                <asp:DropDownList ID="DropDownDepartments" EnableViewState="true" AppendDataBoundItems="true" runat="server">
                                    <asp:ListItem Text="Select the option" disabled="disabled"/>
                                </asp:DropDownList>
                                <div class="select-dropdown"></div>
                            </div>
                         </div>
                        
                        <%--<div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="class">
                                    <option disabled="disabled" selected="selected">CLASS</option>
                                    <option>Class 1</option>
                                    <option>Class 2</option>
                                    <option>Class 3</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>--%>
                        
                        <div class="p-t-20">
                            <%--<button class="btn btn--radius btn--green" type="submit">Submit</button>--%>
                            <asp:Button Text="Register Task" ID="btnTaskRegister" CssClass="btn btn--radius btn--green" runat="server" type="submit" onclick="taskRegister_click" />
                        </div>
                        <%--<div class="p-t-20">
                            <%--<button class="btn btn--radius btn--green" type="submit">Submit</button>--%>
                            <%--<asp:Button Text="Delete Task" ID="BtnTaskDelete" CssClass="btn btn--radius btn--green" runat="server" type="submit" onclick="taskDelete_click" />--%>
                        <%--</div>--%>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>