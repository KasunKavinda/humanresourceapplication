﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using System.Data;

using System.Windows;

namespace HRA
{
    public partial class Employee : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn1;
        MySql.Data.MySqlClient.MySqlCommand cmd1;
        String querystr1;
        String existingEmployeeQuery;

        Boolean empAvailable;
        private int empAvailbaleChecker;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void employeeRegister(object sender, EventArgs e)
        {
            registerEmployee();
        }

        private void registerEmployee()
        {

            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn1 = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn1.Open();
            querystr1 = "";
            existingEmployeeQuery = "";


            //Form validation start
            string messageEmpty = "Please Enter Valid Info.";
            string message = "Your details have been saved successfully.";
            string existingData = "Already Exists.";
            string existingEmpData = "Nic Number Already Exists.";

            string script = "window.onload = function(){ alert('";

            if (firstName.Text == string.Empty || lastName.Text == string.Empty || address.Text == string.Empty || mobileNumber.Text == string.Empty || nicNumber.Text == string.Empty)
            {
                //errorMsg.Visible = true;
                script += messageEmpty;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);


            }
            else
            {
                checkExistEmployee();

                if (empAvailbaleChecker > 0)
                {
                    empAvailable = true;
                }


                if (empAvailable == true)
                {
                    //Emp exist

                    //Emp exist start
                    script += existingEmpData;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                    //Emp exist end
                }
                else if (empAvailable == false)
                {
                    //Emp doesn't exist.

                    if (nicNumber.Text == existingEmployeeQuery)
                    {
                        //can't insert existing data start
                        script += existingData;
                        script += "')};";
                        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                        //can't insert existing data end
                    }
                    else
                    {
                        querystr1 = "INSERT INTO human_resource_application.employee (firstName, lastName, nic_number,  address, contact_detail)" + "VALUES('" + firstName.Text + "', '" + lastName.Text + "', '" + nicNumber.Text + "', '" + address.Text + "', '" + mobileNumber.Text + "')";

                        cmd1 = new MySql.Data.MySqlClient.MySqlCommand(querystr1, conn1);
                        cmd1.ExecuteReader();
                        conn1.Close();


                        //employee fields empty after submit start
                        firstName.Text = string.Empty;
                        lastName.Text = string.Empty;
                        nicNumber.Text = string.Empty;
                        address.Text = string.Empty;
                        mobileNumber.Text = string.Empty;
                        //employee fields empty after submit end


                        //success msg after insert start
                        script += message;
                        script += "')};";
                        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                        //success msg after insert end
                    }
                }
            }
            
        }

        public void checkExistEmployee()
        {
            existingEmployeeQuery = "SELECT nic_number FROM human_resource_application.employee WHERE nic_number='" + nicNumber.Text + "'";
            cmd1 = new MySql.Data.MySqlClient.MySqlCommand(existingEmployeeQuery, conn1);

            MySqlDataAdapter employeeData = new MySqlDataAdapter();
            employeeData.SelectCommand = cmd1;
            DataSet employeeDataSet = new DataSet();
            employeeData.Fill(employeeDataSet);

            empAvailbaleChecker = employeeDataSet.Tables[0].Rows.Count;
        }
    }
}