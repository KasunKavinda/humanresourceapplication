﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace HRA
{
    public partial class task : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        String querystr;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from department", conn);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);  // fill dataset


                DropDownDepartments.DataTextField = ds.Tables[0].Columns["name"].ToString(); // text field name of table dispalyed in dropdown
                DropDownDepartments.DataValueField = ds.Tables[0].Columns["department_id"].ToString();             // to retrive specific  textfield name 
                DropDownDepartments.DataSource = ds.Tables[0];      //assigning datasource to the dropdownlist
                DropDownDepartments.DataBind();  //binding dropdownlist


            }
        }

        protected void taskRegister_click(object sender, EventArgs e)
        {
            registerTask();
        }
        protected void taskDelete_click(object sender, EventArgs e)
        {
            deleteTask();
        }

        private void registerTask()
    {

        String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
        conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
        conn.Open();
        querystr = "";

        //Form validation start
        string messageEmpty = "Please Fill Empty Fields";
        string scriptEmpty = "window.onload = function(){ alert('";

            if (taskTitle.Text == string.Empty || textMaxNoOfEmp.Text == string.Empty || expairyDate.Text == string.Empty || DropDownDepartments.SelectedIndex == 0)
            {
                //errorMsg.Visible = true;
                scriptEmpty += messageEmpty;
                scriptEmpty += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", scriptEmpty, true);
            }
            else if (Int32.Parse(textMaxNoOfEmp.Text) <= 0)
            {
                string ExceedMax = " Please Enter valid number for Maximun NO of employeee field";
                scriptEmpty += ExceedMax;
                scriptEmpty += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", scriptEmpty, true);
            }

            else if (DateTime.Now > Convert.ToDateTime(expairyDate.Text))
            {
                string ExceedMax = " Please Enter valid Expire Date";
                scriptEmpty += ExceedMax;
                scriptEmpty += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", scriptEmpty, true);
            }
        
        else
        {
            
            int selectedId = Int32.Parse(DropDownDepartments.SelectedItem.Value);
            querystr = "INSERT INTO human_resource_application.task (task_name, maximum_no_of_emp_allow, expire_date, department_id, task_description)" + "VALUES('" + taskTitle.Text + "', '" + textMaxNoOfEmp.Text + "','" + expairyDate.Text + "','"+ selectedId+ "' ,'" + taskDescription.Text + "')";
            

            cmd = new MySql.Data.MySqlClient.MySqlCommand(querystr, conn);
            cmd.ExecuteReader();
            conn.Close();


            string message = "Your details have been saved successfully.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            //document.getElementById("taskRegistrationForm").reset();
            //formObject.reset();

            //reseting to empty fields 
            taskTitle.Text = String.Empty;
            textMaxNoOfEmp.Text = String.Empty;
            expairyDate.Text = String.Empty;
            taskDescription.Text = String.Empty;





        }


    }

    public void deleteTask()
    {
        string messageEmpty = "Please fill both the Tile of the task and the Department Id Fields";
        string scriptEmpty = "window.onload = function(){ alert('";

        if (taskTitle.Text == string.Empty || DropDownDepartments.SelectedValue == String.Empty)
        {
            
            scriptEmpty += messageEmpty;
            scriptEmpty += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", scriptEmpty, true);


        }
        else
        {

            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            querystr = "";

            
            querystr = "DELETE FROM human_resource_application.task where (task.task_name = '" + taskTitle.Text + "')  and (task.department_id = '" + Int32.Parse(DropDownDepartments.SelectedItem.Value)+ "') ";
            cmd = new MySql.Data.MySqlClient.MySqlCommand(querystr, conn);
            cmd.ExecuteReader();
            conn.Close();

            string message = "Task is deleted successfully.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }

    }
    }
}