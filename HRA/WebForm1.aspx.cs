﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using System.Data;

using System.Windows;


namespace HRA
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        String querystr;
        String existingDepartmentQuery;
        //String check_Dept_Name;
        Boolean deptAvailable;
        private int deptAvailbaleChecker;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            registerUser();
        }

        private void registerUser()
        {
            
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            querystr = "";
            existingDepartmentQuery = "";
            //check_Dept_Name = "";

            //Form validation start
            string messageEmpty = "Please Enter Valid Info.";
            string message = "Your details have been saved successfully.";
            string existingData = "Already Exists.";
            string existingDeptData = "Department Name Already Exists.";

            string script = "window.onload = function(){ alert('";

            if (textName.Text == string.Empty || textAddress.Text == string.Empty)
            {
                //errorMsg.Visible = true;
                script += messageEmpty;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);



            }
            else
            {
                checkExistDepartment();

                if (deptAvailbaleChecker > 0)
                {
                    deptAvailable = true;
                }


                if (deptAvailable == true)
                {
                    //Dept exist

                    //Dept exist start
                    script += existingDeptData;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                    //Dept exist end
                }
                else if(deptAvailable == false)
                {
                    //Dept doesn't exist.

                    if (textName.Text == existingDepartmentQuery)
                    {
                        //can't insert existing data start
                        script += existingData;
                        script += "')};";
                        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                        //can't insert existing data end
                    }
                    else
                    {
                        querystr = "INSERT INTO human_resource_application.department (name, address)" + "VALUES('" + textName.Text + "', '" + textAddress.Text + "')";

                        cmd = new MySql.Data.MySqlClient.MySqlCommand(querystr, conn);
                        cmd.ExecuteReader();
                        conn.Close();


                        //department fields empty after submit start
                        textName.Text = string.Empty;
                        textAddress.Text = string.Empty;
                        //department fields empty after submit end


                        //success msg after insert start
                        script += message;
                        script += "')};";
                        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                        //success msg after insert end
                    }
                }
               }
            
        }

        public void checkExistDepartment()
        {
            existingDepartmentQuery = "SELECT name FROM human_resource_application.department WHERE name='" + textName.Text + "'";
            cmd = new MySql.Data.MySqlClient.MySqlCommand(existingDepartmentQuery, conn);

           

            MySqlDataAdapter departmentData = new MySqlDataAdapter();
            departmentData.SelectCommand = cmd;
            DataSet departmentDataSet = new DataSet();
            departmentData.Fill(departmentDataSet);

            deptAvailbaleChecker = departmentDataSet.Tables[0].Rows.Count;
        }

       
    }
}