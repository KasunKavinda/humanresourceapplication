﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using System.Data;
using System.Text;

namespace HRA
{
    public partial class adminDashboard : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlCommand cmdDep;
        MySql.Data.MySqlClient.MySqlCommand cmdTask;
        MySql.Data.MySqlClient.MySqlCommand cmdUpdatedEmployee;
        MySql.Data.MySqlClient.MySqlCommand cmdUpdatedEmployeeTask;
        MySql.Data.MySqlClient.MySqlCommand cmdSelectedEmployee;
        MySql.Data.MySqlClient.MySqlCommand cmdDisabledEmployee;
        MySql.Data.MySqlClient.MySqlCommand cmdUpdatedEmployeeFname;
        MySql.Data.MySqlClient.MySqlCommand cmdUpdatedEmployeeLname;

        private string queryEmpList;
        private string queryDepList;
        private string queryTaskList;
        private string queryUpdatedEmployee;
        private string QueryUpdatedEmployeeDepartment_id;
        private string QueryUpdatedEmployeeTask_id;
        private string QuerySelectedEmployeeDepartment;
        private string queryDisabledEmployee;
        private string QueryDisabledEmployeeDepartment;

        StringBuilder disButton = new StringBuilder();
        private string queryEmpListFname;
        private string queryEmpListLname;
        private string QueryUpdatedEmployeeFname;
        private string QueryUpdatedEmployeeLname;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dropDownLoader();
            }
            
        }
        
        protected void loadRelevantTask(object sender, EventArgs e)
        {

            ////////////// need codes
            //String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            //conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            //conn.Open();

            //string depTemp = DropDownDept.SelectedItem.Text;
            //queryTaskList = "SELECT * FROM department d inner join task t on d.department_id = t.department_id  WHERE(name = '" + depTemp + "')";
            //cmdTask = new MySql.Data.MySqlClient.MySqlCommand(queryTaskList, conn);
            ////Task list start
            //MySqlDataAdapter daTask = new MySqlDataAdapter(cmdTask);
            //daTask.SelectCommand = cmdTask;
            //DataSet dsTask = new DataSet();
            //daTask.Fill(dsTask);  // fill dataset
            ////////////// need codes


            ////////////// need codes
            //DropDownTask.DataTextField = dsTask.Tables[0].Columns["task_name"].ToString(); // text field name of table dispalyed in dropdown
            //DropDownTask.DataValueField = dsTask.Tables[0].Columns["task_name"].ToString();             // to retrive specific  textfield name 
            //DropDownTask.DataSource = dsTask.Tables[0];      //assigning datasource to the dropdownlist
            //DropDownTask.DataBind();  //binding dropdownlist
            ////////////// need codes
            //Task list end
        }

        void dropDownLoader()
        {
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();

            queryEmpList = "select nic_number FROM human_resource_application.employee e LEFT JOIN human_resource_application.department d ON e.department_id=d.department_id WHERE d.department_id IS NULL";
            //queryEmpList = "select nic_number FROM human_resource_application.employee e LEFT JOIN human_resource_application.department d ON e.department_id=d.department_id LEFT JOIN human_resource_application.task t ON e.task_id=t.task_id WHERE d.department_id IS NULL || t.task_id IS NULL";



            queryDepList = "select name from human_resource_application.department";
            //queryTaskList = "select task_name from human_resource_application.task";
            
            

            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryEmpList, conn);
            cmdDep = new MySql.Data.MySqlClient.MySqlCommand(queryDepList, conn);
            

            //Employee list start
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset

            DropDownEmployees.DataTextField = ds.Tables[0].Columns["nic_number"].ToString(); // text field name of table dispalyed in dropdown
            DropDownEmployees.DataValueField = ds.Tables[0].Columns["nic_number"].ToString();             // to retrive specific  textfield name 
            DropDownEmployees.DataSource = ds.Tables[0];      //assigning datasource to the dropdownlist
            DropDownEmployees.DataBind();  //binding dropdownlist
            //Employee list end

            //Department list start
            MySqlDataAdapter daDep = new MySqlDataAdapter(cmdDep);
            daDep.SelectCommand = cmdDep;
            DataSet dsDep = new DataSet();
            daDep.Fill(dsDep);  // fill dataset

            DropDownDept.DataTextField = dsDep.Tables[0].Columns["name"].ToString(); // text field name of table dispalyed in dropdown
            DropDownDept.DataValueField = dsDep.Tables[0].Columns["name"].ToString();             // to retrive specific  textfield name 
            DropDownDept.DataSource = dsDep.Tables[0];      //assigning datasource to the dropdownlist
            DropDownDept.DataBind();  //binding dropdownlist
            //Department list end

            
        }

        protected void employeeAssign(object sender, EventArgs e)
        {
            if (DropDownEmployees.SelectedIndex == 0 || DropDownDept.SelectedIndex==0)
            {
                string existNic = "Please Insert The Nic Number and Department Name .";

                string script = "window.onload = function(){ alert('";
                //errorMsg.Visible = true;
                script += existNic;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
            else
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                //getting dep ID
                queryUpdatedEmployee = "SELECT department_id FROM human_resource_application.department WHERE name='" + DropDownDept.SelectedItem.Value + "'";
                cmdUpdatedEmployee = new MySql.Data.MySqlClient.MySqlCommand(queryUpdatedEmployee, conn);

                MySqlDataAdapter daQueryUpdatedEmployee = new MySqlDataAdapter();
                daQueryUpdatedEmployee.SelectCommand = cmdUpdatedEmployee;
                DataSet updatedEmployeeDataSet = new DataSet();
                daQueryUpdatedEmployee.Fill(updatedEmployeeDataSet);
                QueryUpdatedEmployeeDepartment_id = updatedEmployeeDataSet.Tables[0].Rows[0]["department_id"].ToString();

                //getting task ID

                ////////////// need codes
                //queryUpdatedEmployee = "SELECT task_id FROM human_resource_application.task WHERE task_name='" + DropDownTask.SelectedItem.Value + "'";

                //cmdUpdatedEmployeeTask = new MySql.Data.MySqlClient.MySqlCommand(queryUpdatedEmployee, conn);

                //MySqlDataAdapter daUpdatedEmployeeTask = new MySqlDataAdapter();
                //daUpdatedEmployeeTask.SelectCommand = cmdUpdatedEmployeeTask;
                //DataSet updatedEmployeeTaskDataSet = new DataSet();
                //daUpdatedEmployeeTask.Fill(updatedEmployeeTaskDataSet);
                //QueryUpdatedEmployeeTask_id = updatedEmployeeTaskDataSet.Tables[0].Rows[0]["task_id"].ToString();
                ////////////// need codes

                //update query

                //string updatingEmployee = "UPDATE human_resource_application.employee SET department_id=" + QueryUpdatedEmployeeDepartment_id + ",task_id=" + QueryUpdatedEmployeeTask_id + " WHERE nic_number='" + DropDownEmployees.SelectedItem.Value + "'";
                //string updatingEmployee = "UPDATE human_resource_application.employee SET department_id=" + QueryUpdatedEmployeeDepartment_id + "WHERE nic_number='" + DropDownEmployees.SelectedItem.Value + "'";
                string updatingEmployee = "UPDATE human_resource_application.employee SET department_id=" + QueryUpdatedEmployeeDepartment_id + " WHERE nic_number='" + DropDownEmployees.SelectedItem.Value + "'";


                cmd = new MySql.Data.MySqlClient.MySqlCommand(updatingEmployee, conn);
                cmd.ExecuteReader();
                conn.Close();

                Response.Redirect("TaskAssignment.aspx");
            }
            
        }

        protected void selectingName(object sender, EventArgs e)
        {
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();

            //getting dep ID
            //queryUpdatedEmployee = "SELECT nic_number FROM human_resource_application.employee WHERE firstName='" + queryEmpListFname + "' and lastName='" + queryEmpListLname + "'";
            

            


            queryUpdatedEmployee = "SELECT CONCAT(firstName, ' ' , lastName) AS fullNAme FROM human_resource_application.employee WHERE nic_number = '" + DropDownEmployees.SelectedItem.Value + "'";


            cmdSelectedEmployee = new MySql.Data.MySqlClient.MySqlCommand(queryUpdatedEmployee, conn);

            MySqlDataAdapter daQuerySelectedEmployee = new MySqlDataAdapter();
            daQuerySelectedEmployee.SelectCommand = cmdSelectedEmployee;
            DataSet selectedEmployeeDataSet = new DataSet();
            daQuerySelectedEmployee.Fill(selectedEmployeeDataSet);
            QuerySelectedEmployeeDepartment = selectedEmployeeDataSet.Tables[0].Rows[0]["fullNAme"].ToString();

            fullName.Text = QuerySelectedEmployeeDepartment;

            //disabling existing employee department
            queryDisabledEmployee = "SELECT department_id FROM human_resource_application.employee WHERE nic_number = '" + DropDownEmployees.SelectedItem.Value + "'";
            cmdDisabledEmployee = new MySql.Data.MySqlClient.MySqlCommand(queryDisabledEmployee, conn);

            MySqlDataAdapter daQueryDisabledEmployee = new MySqlDataAdapter();
            daQueryDisabledEmployee.SelectCommand = cmdDisabledEmployee;
            DataSet DisableEmployeeDataSet = new DataSet();
            daQueryDisabledEmployee.Fill(DisableEmployeeDataSet);
            QueryDisabledEmployeeDepartment = DisableEmployeeDataSet.Tables[0].Rows[0]["department_id"].ToString();

            if (QueryDisabledEmployeeDepartment == "")
            {
                
                DropDownDept.Attributes.Remove("disabled");

            }
            else
            {
                DropDownDept.Attributes.Add("disabled", "disabled");
            }

        }
    }
}