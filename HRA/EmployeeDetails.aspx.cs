﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using System.Data;

using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;

namespace HRA
{
    public partial class EmployeeDetails : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmdEmployeeDetails;
        MySql.Data.MySqlClient.MySqlCommand cmdEmployeeDeps;
        private string queryEmployeeDetails;


        StringBuilder table = new StringBuilder();
        private string queryEmployeeDeps;

        protected void Page_Load(object sender, EventArgs e)
        {
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();

            queryEmployeeDetails = "SELECT e.nic_number, e.firstName, e.lastName, e.address, e.contact_detail, d.name, t.task_name  FROM human_resource_application.department d JOIN employee e ON d.department_id=e.department_id JOIN task t ON t.task_id=e.task_id";
            cmdEmployeeDetails = new MySql.Data.MySqlClient.MySqlCommand(queryEmployeeDetails, conn);
            MySqlDataReader rd = cmdEmployeeDetails.ExecuteReader();

            table.Append("<table class='table'>");
            table.Append("<thead class='thead-dark'>");
            table.Append("<tr><th>Nic Number</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Contact Details</th><th>Department</th><th>Task</th>");
            table.Append("</tr>");

            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    table.Append("<tr>");
                    table.Append("<td>" + rd[0] + "</td>");
                    table.Append("<td>" + rd[1] + "</td>");
                    table.Append("<td>" + rd[2] + "</td>");
                    table.Append("<td>" + rd[3] + "</td>");
                    table.Append("<td>" + rd[4] + "</td>");
                    table.Append("<td>" + rd[5] + "</td>");
                    table.Append("<td>" + rd[6] + "</td>");

                    table.Append("</tr>");
                }
            }
            table.Append("</thead>");
            table.Append("</table>");

            PlaceHolder1.Controls.Add(new Literal { Text = table.ToString() });
            rd.Close();
        }

        
    }
}