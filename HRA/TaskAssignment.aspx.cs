﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRA
{
    public partial class TaskAssignment : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        String querystr;
        protected void Page_Load(object sender, EventArgs e)
        {
            loadDepartments();
        }
        protected void load_relevant_tasks(object sender, EventArgs e)
        {
            
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();


                string depTemp = DropDownDepart.SelectedItem.Text;

                //string sqlquery = ("SELECT * FROM department d inner join task t on d.department_id=t.department_id  WHERE (name = '" + depTemp + "')");
                String sqlquery = ("SELECT *,CONCAT(task_name,'- ', expire_date) AS label FROM department d inner join task t on d.department_id=t.department_id where (DATEDIFF(expire_date,CURDATE()))>=0 and (name = '" + depTemp + "')");
                MySqlCommand command = new MySqlCommand(sqlquery, conn);

                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);  // fill dataset

                ds.Tables[0].Columns["task_name"].ToString();

                DropDownTaskList.DataTextField = ds.Tables[0].Columns["label"].ToString(); // text field name of table dispalyed in dropdown
                DropDownTaskList.DataValueField = ds.Tables[0].Columns["task_id"].ToString();             // to retrive specific  textfield name 
                DropDownTaskList.DataSource = ds.Tables[0];      //assigning datasource to the dropdownlist
                DropDownTaskList.DataBind();  //binding dropdownlist

                //int milliseconds = 2000;
                //Thread.Sleep(milliseconds);
                //DropDownTaskList.Items.Clear();





        }
        protected void loadDepartments()
        {
            if (!IsPostBack)
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from department", conn);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);  // fill dataset

                DropDownDepart.DataTextField = ds.Tables[0].Columns["name"].ToString(); // text field name of table dispalyed in dropdown
                DropDownDepart.DataValueField = ds.Tables[0].Columns["department_id"].ToString();             // to retrive specific  textfield name 
                DropDownDepart.DataSource = ds.Tables[0];      //assigning datasource to the dropdownlist
                DropDownDepart.DataBind();  //binding dropdownlist

            }

        }
        protected void load_employeesToCheckboxes(object sender, EventArgs e)
        {
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();


            string depId = DropDownDepart.SelectedItem.Value;
            String taskId = DropDownTaskList.SelectedItem.Value;

            string sqlquery = ("SELECT *,CONCAT(firstName,' ', lastName,'-',nic_number) AS empDis FROM employee e   WHERE (department_id = '" + depId + "') AND (task_id is null)");
            MySqlCommand command = new MySqlCommand(sqlquery, conn);

            MySqlDataAdapter da = new MySqlDataAdapter(command);
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);  //fill dataset


            CheckBoxListEmployee.DataTextField = ds.Tables[0].Columns["empDis"].ToString();
            CheckBoxListEmployee.DataValueField = ds.Tables[0].Columns["empId"].ToString();
            CheckBoxListEmployee.DataSource = ds.Tables[0];
            CheckBoxListEmployee.DataBind();

            

        }


        protected void AssignTask_Click(object sender, EventArgs e)
        {
            string messageEmpty = "Please fill both the Tile of the task and the Department Id Fields";
            string scriptEmpty = "window.onload = function(){ alert('";

            if (DropDownDepart.SelectedIndex == 0 || DropDownTaskList.SelectedIndex == 0)
            {

                scriptEmpty += messageEmpty;
                scriptEmpty += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", scriptEmpty, true);


            }
            else
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbconnection"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);

                string checkedEmpId = "";
                String selectedDep = DropDownDepart.SelectedItem.Value;
                String selectedTask = DropDownTaskList.SelectedItem.Value;


                for (int i = 0; i < CheckBoxListEmployee.Items.Count; i++)
                {
                    if (CheckBoxListEmployee.Items[i].Selected)
                    {
                        checkedEmpId = CheckBoxListEmployee.Items[i].Value;
                        //k = k + CheckBoxList1.Items[i].Text + "</br>";
                        conn.Open();
                        querystr = "UPDATE human_resource_application.employee SET task_id='" + selectedTask + "' where empId='" + checkedEmpId + "' AND department_id='" + selectedDep + "' ";

                        cmd = new MySql.Data.MySqlClient.MySqlCommand(querystr, conn);
                        cmd.ExecuteReader();
                        conn.Close();

                    }

                }
                string message = "This task is assigned successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

                Response.Redirect("EmployeeDetails.aspx");
            }

        }

    }
}